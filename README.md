#Overview

This is a financial application developed with;

 - Java 8
 - SpringBoot 2.1.3
 - Mysql
 - Hibernate
 - Thymeleaf
 - Devtools
 - Tomcat
 - Maven
 - JavaScript
 - Jquery
 - Bootstrap

#Quickstart
 - Make a pull request as a Maven Project
 - Insert the database information in the application properties file _(I recommend this service, 5mb MySql, perfect for testing.)_    [gearhost](https://www.gearhost.com/)
 - Access the application at http://localhost:8080.
 
#Prints
 
 ![Mockup for feature A](https://i.ibb.co/mB4NnbD/1.png)
 
 ![Mockup for feature A](https://i.ibb.co/0XpvWNk/2.png)
 
 ![Mockup for feature A](https://i.ibb.co/D5HS59z/3.png)


