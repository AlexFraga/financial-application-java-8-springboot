package com.financial;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.financial.utilities.CalendarBox;

public class Test {

	public static void main(String[] args) throws ParseException {
		
		Calendar c1 = new GregorianCalendar();
		Calendar c2 = new GregorianCalendar();
		
		String d = "2017-02-25";
		
		c1.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(d));
		c2.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(new CalendarBox().getStringDate()));
		
		System.out.println(c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH));
		System.out.println(c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR));
		
		

	}

}
