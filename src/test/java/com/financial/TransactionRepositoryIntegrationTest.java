package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.enumarations.RecurringPeriod;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Test
	public void whenGetAllByUser_thenReturnListOfTransactions() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));
		User user2 = userRepository.save(new User("Michael", "user2", "pass2"));

		Account account = accountRepository.save(new Account("Bankia", "200.00", true, user1));
		Category category = categoryRepository.save(new Category("Marketing", "0", user1));
		Method method = methodRepository.save(new Method("PayPal", user1));

		transactionRepository.save(new Transaction("400.00", true, "2019-03-20", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category, user1));

		transactionRepository.save(new Transaction("500.00", true, "2019-03-21", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category, user1));

		Assert.assertNotNull(transactionRepository.getAllByUser(user1));
		Assert.assertEquals(2, transactionRepository.getAllByUser(user1).size());
		Assert.assertEquals(0, transactionRepository.getAllByUser(user2).size());
	}

	@Test
	public void whenGetAllByRecurringId_thenReturnListOfTransactions() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));

		Account account = accountRepository.save(new Account("Bankia", "200.00", true, user1));
		Category category = categoryRepository.save(new Category("Marketing", "0", user1));
		Method method = methodRepository.save(new Method("PayPal", user1));

		transactionRepository.save(new Transaction("400.00", true, "2019-03-20", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category, user1));

		transactionRepository.save(new Transaction("500.00", true, "2019-03-21", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category, user1));

		transactionRepository.save(new Transaction("600.00", true, "2019-03-22", "Description", false,
				"recurringId1234", RecurringPeriod.Daily, account, method, category, user1));

		Assert.assertNotNull(transactionRepository.getAllByRecurringId("recurringId123"));
		Assert.assertEquals(2, transactionRepository.getAllByRecurringId("recurringId123").size());
		Assert.assertEquals(0, transactionRepository.getAllByRecurringId("recurringId1235").size());
	}

}
