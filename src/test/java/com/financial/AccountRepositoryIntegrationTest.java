package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.model.Account;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Test
	public void whenGetAllByUser_thenReturnListOfAccount() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));
		User user2 = userRepository.save(new User("Michael", "user2", "pass2"));
		
		accountRepository.save(new Account("Bankia", "200.00", true, user1));
		accountRepository.save(new Account("Sabadell", "300.00", true, user1));
		accountRepository.save(new Account("Bank of Ireland", "700.00", false, user1));

		Assert.assertNotNull(accountRepository.getAllByUser(user1));
		Assert.assertEquals(3, accountRepository.getAllByUser(user1).size());		
		Assert.assertNull(accountRepository.getAllByUser(user2));
	}

}
