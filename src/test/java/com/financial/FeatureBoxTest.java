package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.utilities.FeatureBox;

@RunWith(SpringRunner.class)
public class FeatureBoxTest {

	@Test
	public void whenGetRandomCombinationOfCharacters_thenReturnRandomCombination() {

		String randomString1 = new FeatureBox().getRandomCombinationOfCharacters(8);
		String randomString2 = new FeatureBox().getRandomCombinationOfCharacters(8);

		Assert.assertEquals(8, randomString1.length());
		Assert.assertNotEquals(randomString1, randomString2);

	}

}
