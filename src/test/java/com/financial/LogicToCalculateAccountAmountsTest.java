package com.financial;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.business.LogicToCalculateAccountAmounts;
import com.financial.enumarations.RecurringPeriod;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LogicToCalculateAccountAmountsTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Test
	public void whenCalculateAccountAmounts_thenReturnAccountTotal() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));

		Account account = accountRepository.save(new Account("Bankia", "200.00", true, user1));
		Category category1 = categoryRepository.save(new Category("Marketing", "0", user1)); // income
		Category category2 = categoryRepository.save(new Category("Comercial", "1", user1)); // expense
		Method method = methodRepository.save(new Method("PayPal", user1));

		transactionRepository.save(new Transaction("100.00", true, "2019-03-20", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category1, user1)); // + 100

		transactionRepository.save(new Transaction("200.00", true, "2019-03-21", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category1, user1)); // + 200

		transactionRepository.save(new Transaction("100.00", true, "2019-03-21", "Description", false, "recurringId123",
				RecurringPeriod.Daily, account, method, category2, user1)); // - 100

		Account result = new LogicToCalculateAccountAmounts().calculateAccountAmounts(
				accountRepository.findById(1).get(), transactionRepository.getAllByUser(user1));

		BigDecimal expectedValue = new BigDecimal("400.00");
		Assert.assertEquals(expectedValue, result.getActualBalance()); // must return 400.00
	}

}
