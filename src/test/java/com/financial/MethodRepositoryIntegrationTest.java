package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.model.Method;
import com.financial.model.User;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MethodRepositoryIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private MethodRepository methodRepository;

	@Test
	public void whenGetAllByUser_thenReturnListOfMethod() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));
		User user2 = userRepository.save(new User("Michael", "user2", "pass2"));
		
		methodRepository.save(new Method("PayPal", user1));
		methodRepository.save(new Method("Bank Slip", user1));
		methodRepository.save(new Method("Bank Transfer", user1));

		Assert.assertNotNull(methodRepository.getAllByUser(user1));
		Assert.assertEquals(3, methodRepository.getAllByUser(user1).size());		
		Assert.assertEquals(0, methodRepository.getAllByUser(user2).size());
	}
}
