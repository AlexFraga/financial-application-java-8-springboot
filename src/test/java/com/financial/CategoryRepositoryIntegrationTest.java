package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.model.Category;
import com.financial.model.User;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryRepositoryIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Test
	public void whenGetAllByUser_thenReturnListOfCategory() throws Exception {

		User user1 = userRepository.save(new User("Kevin", "user1", "pass1"));
		User user2 = userRepository.save(new User("Michael", "user2", "pass2"));

		Category category1 = new Category("Marketing", "0", user1);
		categoryRepository.save(category1);

		Category category2 = new Category("Comercial", "0", user1);
		categoryRepository.save(category2);

		Category category3 = new Category("Sales", "1", user1);
		category3.setDeleted(true);
		categoryRepository.save(category3);

		Assert.assertNotNull(categoryRepository.getAllByUser(user1));
		Assert.assertEquals(2, categoryRepository.getAllByUser(user1).size());
		Assert.assertEquals(0, categoryRepository.getAllByUser(user2).size());
	}

}
