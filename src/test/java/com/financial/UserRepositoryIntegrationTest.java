package com.financial;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.financial.model.User;
import com.financial.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Test
	public void whenGetLoginAndPassword_thenReturnUser() throws Exception {

		userRepository.save(new User("Kevin", "user1", "pass1"));

		userRepository.getByLoginAndPassword("Kevin", "user1");

		Assert.assertNotNull(userRepository.getByLoginAndPassword("user1", "pass1"));
		Assert.assertNull(userRepository.getByLoginAndPassword("user1", "pass2"));
	}

}
