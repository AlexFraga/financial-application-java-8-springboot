package com.financial.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.financial.enumarations.RecurringPeriod;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.FeatureBox;

/**
 * Class representing the transactions.
 */
@Entity
@Table(name = "tbl_transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String created; // is a datetime
	private String transactionDueDate; // is a date - is the transaction's due date.
	private String transactionPaymentDate; // is a date - is the date the transaction was paid.
	private String description;

	@Column(precision = 10, scale = 2)
	private BigDecimal value;

	private boolean payOrNotPay;
	private boolean isItRecurring;
	private String recurringId;
	private int quantity;

	@Enumerated(EnumType.ORDINAL)
	private RecurringPeriod recurringPeriod;

	@OneToOne
	private Account account;

	@OneToOne
	private Method method;

	@OneToOne
	private Category category;

	@ManyToOne
	private User user;

	// contructors

	public Transaction() {

	}

	public Transaction(String value, boolean payOrNotPay, String transactionDueDate, String description,
			boolean isItRecurring, String recurringId, RecurringPeriod recurringPeriod, Account account, Method method,
			Category category, User user) {

		FeatureBox featureBox = new FeatureBox();
		CalendarBox calendarBox = new CalendarBox();

		this.created = calendarBox.getStringDateTime();
		this.transactionDueDate = transactionDueDate;
		this.value = featureBox.getMoneyFromHtml(value);
		this.payOrNotPay = payOrNotPay;
		this.description = featureBox.convertUTF8toISO(description);
		this.account = account;
		this.method = method;
		this.category = category;
		this.user = user;
		this.isItRecurring = isItRecurring;
		this.recurringId = recurringId;

		if (payOrNotPay)
			this.transactionPaymentDate = transactionDueDate;
		if (recurringPeriod != null)
			this.recurringPeriod = recurringPeriod;
	}

	// getters and setters

	public String getTransactionDueDate() {
		return transactionDueDate;
	}

	public void setTransactionDueDate(String transactionDueDate) {
		this.transactionDueDate = transactionDueDate;
	}

	public String getTransactionPaymentDate() {
		return transactionPaymentDate;
	}

	public void setTransactionPaymentDate(String transactionPaymentDate) {
		this.transactionPaymentDate = transactionPaymentDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public boolean isPayOrNotPay() {
		return payOrNotPay;
	}

	public void setPayOrNotPay(boolean payOrNotPay) {
		this.payOrNotPay = payOrNotPay;
	}

	public String getRecurringId() {
		return recurringId;
	}

	public void setRecurringId(String recurringId) {
		this.recurringId = recurringId;
	}

	public RecurringPeriod getRecurringPeriod() {
		return recurringPeriod;
	}

	public void setRecurringPeriod(RecurringPeriod recurringPeriod) {
		this.recurringPeriod = recurringPeriod;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public String getCreated() {
		return created;
	}

	public boolean isItRecurring() {
		return isItRecurring;
	}

	public int getQuantity() {
		return quantity;
	}
}
