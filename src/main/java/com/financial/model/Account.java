package com.financial.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.financial.utilities.FeatureBox;

/**
 * Class that represents wallets and bank accounts.
 */
@Entity
@Table(name = "tbl_account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	private boolean includeSumOnDashboard;

	@Column(precision = 10, scale = 2)
	private BigDecimal initialBalance;

	@ManyToOne
	private User user;

	@Transient
	private BigDecimal actualBalance;

	// contructors

	public Account() {

	}

	public Account(String name, String initialBalance, boolean includeSumOnDashboard, User user) { // add

		FeatureBox featureBox = new FeatureBox();

		this.name = featureBox.convertUTF8toISO(name);
		this.includeSumOnDashboard = includeSumOnDashboard;
		this.initialBalance = featureBox.getMoneyFromHtml(initialBalance);
		this.user = user;
	}

	// getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isIncludeSumOnDashboard() {
		return includeSumOnDashboard;
	}

	public void setIncludeSumOnDashboard(boolean includeSumOnDashboard) {
		this.includeSumOnDashboard = includeSumOnDashboard;
	}

	public BigDecimal getInitialBalance() {
		return initialBalance;
	}

	public void setInitialBalance(BigDecimal initialBalance) {
		this.initialBalance = initialBalance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getActualBalance() {
		return actualBalance;
	}

	public void setActualBalance(BigDecimal actualBalance) {
		this.actualBalance = actualBalance;
	}

	public int getId() {
		return id;
	}
}
