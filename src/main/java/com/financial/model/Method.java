package com.financial.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.financial.utilities.FeatureBox;

/**
 * Class representing payment methods.
 */
@Entity
@Table(name = "tbl_method")
public class Method {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	private boolean deleted;

	@ManyToOne
	private User user;

	@Transient
	private BigDecimal paidMonthlyMovement;

	@Transient
	private BigDecimal toPayMonthlyMovement;

	// contructors

	public Method() {

	}

	public Method(String name, User user) {

		FeatureBox featureBox = new FeatureBox();

		this.name = featureBox.convertUTF8toISO(name);
		this.deleted = false;
		this.user = user;
	}

	// getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public BigDecimal getPaidMonthlyMovement() {
		return paidMonthlyMovement;
	}

	public void setPaidMonthlyMovement(BigDecimal paidMonthlyMovement) {
		this.paidMonthlyMovement = paidMonthlyMovement;
	}

	public BigDecimal getToPayMonthlyMovement() {
		return toPayMonthlyMovement;
	}

	public void setToPayMonthlyMovement(BigDecimal toPayMonthlyMovement) {
		this.toPayMonthlyMovement = toPayMonthlyMovement;
	}
}
