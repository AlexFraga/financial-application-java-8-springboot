package com.financial.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.financial.enumarations.Status;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.FeatureBox;

/**
 * Class that represents a user.
 */
@Entity
@Table(name = "tbl_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String created; // is a datetime
	private String name;
	private String login;
	private String password;
	private String avatar;
	private boolean deleted;

	@Enumerated(EnumType.ORDINAL)
	private Status status;

	// contructors

	public User() {

	}

	public User(String name, String login, String password) {

		CalendarBox calendarBox = new CalendarBox();
		FeatureBox featureBox = new FeatureBox();

		this.created = calendarBox.getStringDateTime();
		this.name = featureBox.convertUTF8toISO(name);
		this.login = login;
		this.password = password;
		this.deleted = false;
		this.status = Status.ACTIVE;
	}

	// getters and setters

	public int getId() {
		return id;
	}

	public String getCreated() {
		return created;
	}

	public String getName() {
		return name;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public String getAvatar() {
		return avatar;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public Status getStatus() {
		return status;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
