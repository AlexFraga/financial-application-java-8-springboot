package com.financial.model;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.financial.enumarations.CategoryType;
import com.financial.utilities.FeatureBox;

/**
 * Class representing categories used in transactions.
 */
@Entity
@Table(name = "tbl_category")
public class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	private boolean deleted;

	@Enumerated(EnumType.ORDINAL)
	private CategoryType type;

	@ManyToOne
	private User user;

	@Transient
	private BigDecimal paidMonthlyMovement;

	@Transient
	private BigDecimal toPayMonthlyMovement;

	// contructors

	public Category() {

	}

	public Category(String name, String type, User user) {

		FeatureBox featureBox = new FeatureBox();

		this.name = featureBox.convertUTF8toISO(name);
		this.deleted = false;
		this.user = user;

		this.type = Arrays.stream(CategoryType.values()).filter(e -> e.value == (Integer.parseInt(type) + 1))
				.findFirst().orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", type)));

	}

	// getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public CategoryType getType() {
		return type;
	}

	public void setType(CategoryType type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public BigDecimal getPaidMonthlyMovement() {
		return paidMonthlyMovement;
	}

	public void setPaidMonthlyMovement(BigDecimal paidMonthlyMovement) {
		this.paidMonthlyMovement = paidMonthlyMovement;
	}

	public BigDecimal getToPayMonthlyMovement() {
		return toPayMonthlyMovement;
	}

	public void setToPayMonthlyMovement(BigDecimal toPayMonthlyMovement) {
		this.toPayMonthlyMovement = toPayMonthlyMovement;
	}
}
