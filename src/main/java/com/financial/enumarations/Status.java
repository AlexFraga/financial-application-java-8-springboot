package com.financial.enumarations;

/**
 * ENUM used to define whether the transaction is income or expense
 */
public enum Status {

	ACTIVE(1), INACTIVE(2);

	public int value;

	Status(int value) {
		this.value = value;
	}
}
