package com.financial.enumarations;

/**
 * ENUM used to define recurring transactions
 */
public enum RecurringPeriod {

	Daily(1), Weekly(2), Fortnightly(3), Monthly(4), Quarterly(5), Semiannually(6), Annually(7);

	public int value;

	RecurringPeriod(int value) {
		this.value = value;
	}
}
