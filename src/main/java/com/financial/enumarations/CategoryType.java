package com.financial.enumarations;

/**
 * ENUM used to define recurring transactions
 */
public enum CategoryType {

	Income(1), Expense(2);

	public int value;

	CategoryType(int value) {
		this.value = value;
	}
}
