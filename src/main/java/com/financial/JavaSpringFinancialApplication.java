package com.financial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringFinancialApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringFinancialApplication.class, args);
	}

}
