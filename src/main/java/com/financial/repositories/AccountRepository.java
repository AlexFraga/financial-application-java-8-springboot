package com.financial.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.financial.model.Account;
import com.financial.model.User;

public interface AccountRepository extends CrudRepository<Account, Integer> {

	@Query(value = "SELECT * FROM tbl_account o WHERE o.user_id = :user", nativeQuery = true)
	List<Account> getAllByUser(@Param("user") User user);
}
