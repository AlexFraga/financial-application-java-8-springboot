package com.financial.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.financial.model.Transaction;
import com.financial.model.User;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

	@Query(value = "SELECT * FROM tbl_transaction o WHERE o.user_id = :user", nativeQuery = true)
	List<Transaction> getAllByUser(@Param("user") User user);

	@Query(value = "SELECT * FROM tbl_transaction o WHERE o.recurring_id = :recurringid", nativeQuery = true)
	List<Transaction> getAllByRecurringId(@Param("recurringid") String recurringid);
}
