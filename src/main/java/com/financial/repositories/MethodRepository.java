package com.financial.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.financial.model.Method;
import com.financial.model.User;

public interface MethodRepository extends CrudRepository<Method, Integer> {

	@Query(value = "SELECT * FROM tbl_method o WHERE o.user_id = :user AND deleted = false", nativeQuery = true)
	List<Method> getAllByUser(@Param("user") User user);
}
