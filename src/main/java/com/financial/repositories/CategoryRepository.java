package com.financial.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.financial.model.Category;
import com.financial.model.User;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

	@Query(value = "SELECT * FROM tbl_category o WHERE o.user_id = :user AND deleted = false", nativeQuery = true)
	List<Category> getAllByUser(@Param("user") User user);
}
