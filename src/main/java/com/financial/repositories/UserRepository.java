package com.financial.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.financial.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	@Query(value = "SELECT * FROM tbl_user o WHERE o.login = :login AND o.password = :password", nativeQuery = true)
	User getByLoginAndPassword(@Param("login") String login, @Param("password") String password);
}
