package com.financial.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financial.business.LogicToBuildListOfMonths;
import com.financial.business.LogicToBuildTransaction;
import com.financial.business.LogicToEditTransaction;
import com.financial.business.LogicToValidateUser;
import com.financial.business.BusinessTransaction;
import com.financial.enumarations.CategoryType;
import com.financial.enumarations.RecurringPeriod;
import com.financial.enumarations.Status;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;

@Controller
public class TransactionController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	MethodRepository methodRepository;

	@Autowired
	AccountRepository accountRepository;

	ExceptionBox exceptionBox;

	public TransactionController() {
		this.exceptionBox = new ExceptionBox();
	}

	@GetMapping("/transactions")
	public ModelAndView transactions(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("views/transactions");

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (logged.getStatus() == Status.ACTIVE) {

				modelAndView.addObject("logged", userRepository.findById(logged.getId()).get());
				modelAndView.addObject("months", new LogicToBuildListOfMonths().getListOfMonths());
				modelAndView.addObject("accounts", accountRepository.getAllByUser(logged));
				modelAndView.addObject("methods", methodRepository.getAllByUser(logged));
				modelAndView.addObject("recurrent", RecurringPeriod.values());
				
				modelAndView.addObject("transactions",
						new BusinessTransaction().getTransactions(transactionRepository.getAllByUser(logged)));

				modelAndView.addObject("incomes", categoryRepository.getAllByUser(logged).stream()
						.filter(p -> p.getType() == CategoryType.Income).collect(Collectors.toList()));

				modelAndView.addObject("expenses", categoryRepository.getAllByUser(logged).stream()
						.filter(p -> p.getType() == CategoryType.Expense).collect(Collectors.toList()));

				return modelAndView;

			} else {
				return new ModelAndView("login");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/add-transaction")
	public String addTransaction(@RequestParam("payOrNotPay") boolean payOrNotPay, @RequestParam("value") String value,
			@RequestParam("transactionDueDate") String transactionDueDate,
			@RequestParam("recurrentPeriod") String recurrent, @RequestParam("quantity") int quantity,
			@RequestParam("description") String description, @RequestParam("isItRecurring") boolean isItRecurring,
			@RequestParam("posttype") String postType, @RequestParam("account") String account,
			@RequestParam("category") String category, @RequestParam("method") String method,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				new LogicToBuildTransaction().buildTransaction(payOrNotPay, value, transactionDueDate, recurrent,
						quantity, description, isItRecurring, postType, account, category, method, logged,
						transactionRepository, categoryRepository, methodRepository, accountRepository);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@PostMapping("/edit-transaction")
	public String editTransaction(@RequestParam("id") int id, @RequestParam("value") String value,
			@RequestParam("transactionDueDate") String transactionDueDate, @RequestParam("account") int account,
			@RequestParam("category") int category, @RequestParam("method") int method,
			@RequestParam("payOrNotPay") int payOrNotPay, @RequestParam("description") String description,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				new LogicToEditTransaction().editTransaction(id, payOrNotPay, value, transactionDueDate, description,
						account, category, method, logged, transactionRepository, categoryRepository, methodRepository,
						accountRepository);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@PostMapping("/delete-transaction")
	public String deleteTransaction(@RequestParam("id") int id, @RequestParam("selection") boolean selection,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Transaction transaction = transactionRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				if (selection == true) { // delete only this transaction

					transactionRepository.delete(transaction);

				} else { // delete this and all transactions linked to this transaction

					List<Transaction> list = transactionRepository.getAllByRecurringId(transaction.getRecurringId());
					transactionRepository.deleteAll(list);
				}

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@GetMapping("/gettransaction/{id}")
	public @ResponseBody Transaction getMethodById(@PathVariable("id") int id, HttpServletRequest request) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				return transactionRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

			} else {
				return null;
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}
}
