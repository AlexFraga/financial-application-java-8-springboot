package com.financial.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financial.business.BusinessCategory;
import com.financial.business.LogicToValidateUser;
import com.financial.enumarations.CategoryType;
import com.financial.model.Category;
import com.financial.model.User;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.EnumManagement;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

@Controller
public class CategoryController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	TransactionRepository transactionRepository;

	ExceptionBox exceptionBox;

	@GetMapping("/categories")
	public ModelAndView categories(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("views/categories");

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				modelAndView.addObject("logged", userRepository.findById(logged.getId()).get());
				modelAndView.addObject("categories", new BusinessCategory().getCategories(categoryRepository, transactionRepository, logged));
				modelAndView.addObject("categoryTypes", CategoryType.values());

				return modelAndView;

			} else {
				return new ModelAndView("login");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/add-category")
	public String addCategory(HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				String name = request.getParameter("name");
				String type = request.getParameter("type");

				categoryRepository.save(new Category(name, type, logged));

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}
	
	@PostMapping("/edit-category")
	public String editCategory(@RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("type") String type, HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Category category = categoryRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				category.setName(new FeatureBox().convertUTF8toISO(name));
				category.setType(new EnumManagement().getCategoryType(type));

				categoryRepository.save(category);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@GetMapping("/getcategory/{id}")
	public @ResponseBody Category getCategoryById(@PathVariable("id") int id, HttpServletRequest request) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				return categoryRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

			} else {
				return null;
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/delete-category")
	public String deleteCategory(int id, HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Category category = categoryRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				category.setDeleted(true);

				categoryRepository.save(category);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}
}
