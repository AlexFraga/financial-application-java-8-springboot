package com.financial.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financial.business.LogicToValidateUser;
import com.financial.model.User;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

@Controller
public class UserController {

	@Autowired
	UserRepository userRepository;

	ExceptionBox exceptionBox;

	@PostMapping("/edit-user")
	public String editUser(@RequestParam("name") String name, @RequestParam("login") String login,
			@RequestParam("password") String password, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				User user = userRepository.findById(logged.getId())
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + logged.getId()));

				user.setName(new FeatureBox().convertUTF8toISO(name));
				user.setLogin(login);
				user.setPassword(password);
				userRepository.save(user);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}
}
