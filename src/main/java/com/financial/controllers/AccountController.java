package com.financial.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financial.business.BusinessAccount;
import com.financial.business.LogicToValidateUser;
import com.financial.model.Account;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

@Controller
public class AccountController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	TransactionRepository transactionRepository;

	ExceptionBox exceptionBox;

	@GetMapping("/accounts")
	public ModelAndView accounts(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("views/accounts");

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				modelAndView.addObject("logged", userRepository.findById(logged.getId()).get());
				modelAndView.addObject("accounts", new BusinessAccount().getAccounts(accountRepository, transactionRepository, logged));

				return modelAndView;

			} else {
				return new ModelAndView("login");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/add-account")
	public String addAccount(@RequestParam("name") String name, @RequestParam("initialBalance") String initialBalance,
			@RequestParam("includeSumOnDashboard") boolean includeSumOnDashboard, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				accountRepository.save(new Account(name, initialBalance, includeSumOnDashboard, logged));

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@PostMapping("/edit-account")
	public String editAccount(@RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("initialBalance") String initialBalance,
			@RequestParam("includeSumOnDashboard") boolean includeSumOnDashboard, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Account account = accountRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				account.setName(new FeatureBox().convertUTF8toISO(name));
				account.setInitialBalance(new FeatureBox().getMoneyFromHtml(initialBalance));
				account.setIncludeSumOnDashboard(includeSumOnDashboard);

				accountRepository.save(account);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@GetMapping("/getaccount/{id}")
	public @ResponseBody Account getAccountById(@PathVariable("id") int id, HttpServletRequest request) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				return accountRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

			} else {
				return null;
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/delete-account")
	public String deleteAccount(int id, HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Account account = accountRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
				accountRepository.delete(account);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}
}
