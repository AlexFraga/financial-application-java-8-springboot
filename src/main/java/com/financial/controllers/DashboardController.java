package com.financial.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.financial.business.LogicToBuildListOfMonths;
import com.financial.business.LogicToBuildDashboard;
import com.financial.enumarations.RecurringPeriod;
import com.financial.enumarations.Status;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;

@Controller
public class DashboardController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	MethodRepository methodRepository;

	@Autowired
	AccountRepository accountRepository;

	ExceptionBox exceptionBox;

	public DashboardController() {
		this.exceptionBox = new ExceptionBox();
	}

	@GetMapping("/dashboard")
	public ModelAndView dashboard(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("views/dashboard");

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (logged.getStatus() == Status.ACTIVE) {

				List<Object> dashboard = new LogicToBuildDashboard().getDashboardData(accountRepository,
						transactionRepository, categoryRepository, methodRepository, logged);

				modelAndView.addObject("logged", userRepository.findById(logged.getId()).get());
				modelAndView.addObject("months", new LogicToBuildListOfMonths().getListOfMonths());

				modelAndView.addObject("accounts", dashboard.get(0));
				modelAndView.addObject("statistics", dashboard.get(1));
				modelAndView.addObject("incomes", dashboard.get(2));
				modelAndView.addObject("expenses", dashboard.get(3));
				modelAndView.addObject("categories", dashboard.get(4));
				modelAndView.addObject("methods", dashboard.get(5));
				modelAndView.addObject("transactions", dashboard.get(6));

				modelAndView.addObject("recurrent", RecurringPeriod.values());

				return modelAndView;

			} else {
				return new ModelAndView("login");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}
}
