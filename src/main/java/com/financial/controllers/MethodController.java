package com.financial.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.financial.business.BusinessMethod;
import com.financial.business.LogicToValidateUser;
import com.financial.model.Method;
import com.financial.model.User;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

@Controller
public class MethodController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	MethodRepository methodRepository;
	
	@Autowired
	TransactionRepository transactionRepository;

	ExceptionBox exceptionBox;

	@GetMapping("/methods")
	public ModelAndView methods(HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("views/methods");

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				modelAndView.addObject("logged", userRepository.findById(logged.getId()).get());
				modelAndView.addObject("methods", new BusinessMethod().getMethods(methodRepository, transactionRepository, logged));

				return modelAndView;

			} else {
				return new ModelAndView("login");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@PostMapping("/add-method")
	public String addMethod(@RequestParam("name") String name, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				methodRepository.save(new Method(name, logged));

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@PostMapping("/edit-method")
	public String editMethod(@RequestParam("id") int id, @RequestParam("name") String name, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Method method = methodRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				method.setName(new FeatureBox().convertUTF8toISO(name));

				methodRepository.save(method);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}

	@GetMapping("/getmethod/{id}")
	public @ResponseBody Method getMethodById(@PathVariable("id") int id, HttpServletRequest request) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				return methodRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

			} else {
				return null;
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/delete-method")
	public String deleteMethod(int id, HttpServletRequest request, RedirectAttributes redirectAttributes) {

		try {

			User logged = (User) request.getSession().getAttribute("logging");

			if (new LogicToValidateUser().validateUser(userRepository, logged)) {

				Method method = methodRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				method.setDeleted(true);

				methodRepository.save(method);

				redirectAttributes.addFlashAttribute("text", "Operation performed successfully.");
				redirectAttributes.addFlashAttribute("message", "1");

				return "redirect:" + request.getHeader("Referer");

			} else {

				redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
				redirectAttributes.addFlashAttribute("message", "3");
				return "redirect:" + request.getHeader("Referer");
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();

			redirectAttributes.addFlashAttribute("text", "An error has occurred, please try again!");
			redirectAttributes.addFlashAttribute("message", "2");
			return "redirect:" + request.getHeader("Referer");
		}
	}
}
