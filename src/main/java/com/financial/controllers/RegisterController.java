package com.financial.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.financial.business.LogicToInsertingInictialData;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;

@Controller
public class RegisterController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	MethodRepository methodRepository;

	@Autowired
	AccountRepository accountRepository;

	ExceptionBox exceptionBox;

	public RegisterController() {
		this.exceptionBox = new ExceptionBox();
	}

	@GetMapping("/register")
	public String register() {

		return "views/register";
	}

	@PostMapping("/creating-user")
	public String creatingUser(@RequestParam("name") String name, @RequestParam("login") String login,
			@RequestParam("password1") String password1, @RequestParam("password2") String password2,
			HttpSession session, HttpServletRequest request) {

		try {

			if (password1.equals(password2)) {
				
				User user = userRepository.save(new User(name, login, password1));
				
				new LogicToInsertingInictialData().insertingInictialData(user, accountRepository, transactionRepository,
						categoryRepository, methodRepository);

				return "redirect:login";

			} else {
				return "redirect:register";
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}
}
