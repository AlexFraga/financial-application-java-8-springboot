package com.financial.controllers;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;

@Controller
public class LoginController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	MethodRepository methodRepository;

	@Autowired
	AccountRepository accountRepository;

	ExceptionBox exceptionBox;

	public LoginController() {
		this.exceptionBox = new ExceptionBox();
	}

	@GetMapping(value = { "/", "/login" })
	public String login() {

		return "views/login";
	}

	@PostMapping("/logging-in")
	public String loggingIn(@RequestParam("login") String login, @RequestParam("password") String password,
			HttpSession session, HttpServletRequest request) {

		try {

			User logged = userRepository.getByLoginAndPassword(login, password);

			if (logged != null) {
				session.setAttribute("logging", logged);
				request.getSession().getAttribute("logging");

				return "redirect:dashboard";

			} else {
				return "redirect:login";
			}

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) throws IOException {

		request.getSession().removeAttribute("logging");

		HttpSession session = request.getSession(false);
		if (request.isRequestedSessionIdValid() && session != null) {
			session.invalidate();
		}
		handleLogOutResponse(response, request);
		return "redirect:login";

	}

	private void handleLogOutResponse(HttpServletResponse response, HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			cookie.setMaxAge(0);
			cookie.setValue(null);
			cookie.setPath("/");
			response.addCookie(cookie);
		}
	}
}
