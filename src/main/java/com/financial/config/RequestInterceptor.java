package com.financial.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {

		String uri = request.getRequestURI();
		       if (uri.endsWith("login")
				|| uri.endsWith("logging-in")
				|| uri.endsWith("register")
				|| uri.endsWith("creating-user")
				
				|| uri.contains("assets")
				|| uri.contains("dist")) {
			return true;
		}

		if (request.getSession().getAttribute("logging") != null) {
			return true;
		} else {
			response.sendRedirect("login");
			return false;
		}

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView model)
			throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception arg3)
			throws Exception {
	}
}
