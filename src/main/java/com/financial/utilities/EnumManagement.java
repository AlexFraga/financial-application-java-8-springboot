package com.financial.utilities;

import java.util.Arrays;

import com.financial.enumarations.CategoryType;
import com.financial.enumarations.RecurringPeriod;

public class EnumManagement {

	/**
	 * Returns a RecurringPeriod by getting a String for the value of the enum
	 * 
	 * @author Rodrigo Farias
	 * @return RecurringPeriod
	 */
	public RecurringPeriod getRecurrentPeriod(String recurrentPeriod) {

		return Arrays.stream(RecurringPeriod.values()).filter(e -> e.value == (Integer.parseInt(recurrentPeriod) + 1))
				.findFirst()
				.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", recurrentPeriod)));
	}

	/**
	 * Returns a CategoryType by getting a String for the value of the enum
	 * 
	 * @author Rodrigo Farias
	 * @return RecurringPeriod
	 */
	public CategoryType getCategoryType(String categoryType) {

		return Arrays.stream(CategoryType.values()).filter(e -> e.value == (Integer.parseInt(categoryType) + 1))
				.findFirst()
				.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", categoryType)));
	}
}
