package com.financial.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class CalendarBox {

	ExceptionBox exceptionBox;

	// constructor
	public CalendarBox() {
		this.exceptionBox = new ExceptionBox();
	}

	/**
	 * Returns a string Datetime e.g ( yyyy-MM-dd HH:mm:ss ).
	 * 
	 * @author Rodrigo Farias
	 * @return String Datetime using (America/Sao_Paulo) Timezone
	 */
	public String getStringDateTime() {

		try {

			Calendar calendar = new GregorianCalendar();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
			return dateFormat.format(calendar.getTime());

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns a string Datetime e.g ( yyyy-MM-dd ).
	 * 
	 * @author Rodrigo Farias
	 * @return String Date using (America/Sao_Paulo) Timezone
	 */
	public String getStringDate() {

		try {

			Calendar calendar = new GregorianCalendar();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
			return dateFormat.format(calendar.getTime());

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns a string Datetime e.g ( HH:mm:ss ).
	 * 
	 * @author Rodrigo Farias
	 * @return String Time using (America/Sao_Paulo) Timezone
	 */
	public String getStringTime() {

		try {

			Calendar calendar = new GregorianCalendar();
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
			return dateFormat.format(calendar.getTime());

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Convert a String date (yyyy-MM-dd) to (dd/MM/yyyy)
	 *
	 * @author Rodrigo Farias
	 * @return String Date
	 */
	public String convertStringDate(String date) {

		try {

			Date from = new SimpleDateFormat("yyyy-MM-dd").parse(date);
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			return dateFormat.format(from);

		} catch (ParseException e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Compare if month and year are current, receive a (yyyy-MM-dd)
	 *
	 * @author Rodrigo Farias
	 * @return boolean
	 */
	public boolean compareMonthAndYearAreCurrent(String date) {

		try {

			Calendar c1 = new GregorianCalendar();
			Calendar c2 = new GregorianCalendar();

			c1.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
			c2.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(getStringDate()));

			if (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)) {
				return true;
			}

			return false;

		} catch (ParseException e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return false;
		}
	}
}
