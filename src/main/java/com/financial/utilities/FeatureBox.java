package com.financial.utilities;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

public class FeatureBox {

	ExceptionBox exceptionBox;

	// constructor
	public FeatureBox() {
		this.exceptionBox = new ExceptionBox();
	}

	/**
	 * Receives a String BigDecimal, usually from a Request and converts to
	 * BigDecimal.
	 *
	 * @author Rodrigo Farias
	 * @return BigDecimal
	 */
	public BigDecimal getMoneyFromHtml(String amount) {

		if (!"".equals(amount)) {

			try {

				DecimalFormatSymbols symbols = new DecimalFormatSymbols();
				symbols.setGroupingSeparator(',');
				symbols.setDecimalSeparator('.');
				String pattern = "#,##0.0#";
				DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
				decimalFormat.setParseBigDecimal(true);
				BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(amount);
				return bigDecimal;

			} catch (Exception e) {
				exceptionBox.getAnExceptionAndDoTheTreatment(e);
				e.printStackTrace();
				return new BigDecimal("0.0");
			}

		} else {
			return new BigDecimal("0.0");
		}
	}

	/**
	 * Converts a UTF-8 String to ISO-8859-1.
	 *
	 * @author Rodrigo Farias
	 * @return Converted String ISO-8859-1
	 */
	public String convertUTF8toISO(String string) {

		try {

			Charset utf8charset = Charset.forName("UTF-8");
			Charset iso88591charset = Charset.forName("ISO-8859-1");
			ByteBuffer inputBuffer = ByteBuffer.wrap(string.getBytes());
			CharBuffer data = utf8charset.decode(inputBuffer);
			ByteBuffer outputBuffer = iso88591charset.encode(data);
			byte[] outputData = outputBuffer.array();

			return new String(outputData);

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Generates a random String with a specific amount of characters.
	 *
	 * @author Rodrigo Farias
	 * @return Random String Combination
	 */
	public String getRandomCombinationOfCharacters(int numberOfCharacters) {

		try {

			char[] chart = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
					'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
			char[] pass = new char[numberOfCharacters];
			int chartLenght = chart.length;
			Random rdm = new Random();
			for (int x = 0; x < numberOfCharacters; x++) {
				pass[x] = chart[rdm.nextInt(chartLenght)];
			}
			return new String(pass);

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns a boolean if the string can be converted to int
	 *
	 * @author Rodrigo Farias
	 * @return boolean
	 */
	public boolean isNumeric(String strNum) {
		try {
			Integer.parseInt(strNum);
		} catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}

}
