package com.financial.utilities;

public class ExceptionBox {

	/**
	 * Receive an exception and do the treatment..
	 * 
	 * @author Rodrigo Farias
	 * @return Void
	 */
	public void getAnExceptionAndDoTheTreatment(Throwable exception) {
		
		// usually I email myself for monitoring

	}
}