package com.financial.business;

import java.util.List;

import com.financial.model.Method;
import com.financial.model.User;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.ExceptionBox;

public class BusinessMethod {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of methods with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Method>
	 */
	public List<Method> getMethods(MethodRepository methodRepository,
			TransactionRepository transactionRepository, User logged) {

		return new LogicToCalculateMethodAmounts().getMethodsWithCalculatedAmounts(
				methodRepository.getAllByUser(logged), transactionRepository.getAllByUser(logged));
	}
}
