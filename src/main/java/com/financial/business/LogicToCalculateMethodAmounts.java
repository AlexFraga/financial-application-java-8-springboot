package com.financial.business;

import java.math.BigDecimal;
import java.util.List;

import com.financial.model.Method;
import com.financial.model.Transaction;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;

public class LogicToCalculateMethodAmounts {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of methods with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Method>
	 */
	public List<Method> getMethodsWithCalculatedAmounts(List<Method> methods,
			List<Transaction> transactions) {

		for (Method object : methods) {
			calculateMethodAmounts(object, transactions);
		}

		return methods;
	}

	/**
	 * Get a Method with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return Method
	 */
	public Method calculateMethodAmounts(Method Method, List<Transaction> transactions) {
		
		BigDecimal paidMonthlyMovement = transactions
				.stream()
				.filter(p -> p.getMethod().getId() == Method.getId())
				.filter(p -> p.isPayOrNotPay())
				.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
				.map(Transaction::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		BigDecimal toPayMonthlyMovement = transactions
				.stream()
				.filter(p -> p.getMethod().getId() == Method.getId())
				.filter(p -> p.isPayOrNotPay() == false)
				.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
				.map(Transaction::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		Method.setPaidMonthlyMovement(paidMonthlyMovement);
		Method.setToPayMonthlyMovement(toPayMonthlyMovement);

		return Method;
	}
}
