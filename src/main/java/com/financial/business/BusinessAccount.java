package com.financial.business;

import java.util.List;

import com.financial.model.Account;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.ExceptionBox;

public class BusinessAccount {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of accounts with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Account>
	 */
	public List<Account> getAccounts(AccountRepository accountRepository, TransactionRepository transactionRepository,
			User logged) {

		return new LogicToCalculateAccountAmounts().getAccountsWithCalculatedAmounts(
				accountRepository.getAllByUser(logged), transactionRepository.getAllByUser(logged));
	}
}
