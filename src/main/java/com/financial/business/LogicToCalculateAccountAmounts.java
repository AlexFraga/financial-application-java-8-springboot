package com.financial.business;

import java.math.BigDecimal;
import java.util.List;

import com.financial.enumarations.CategoryType;
import com.financial.model.Account;
import com.financial.model.Transaction;
import com.financial.utilities.ExceptionBox;

public class LogicToCalculateAccountAmounts {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of accounts with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Account>
	 */
	public List<Account> getAccountsWithCalculatedAmounts(List<Account> accounts, List<Transaction> transactions) {

		for (Account object : accounts) {
			calculateAccountAmounts(object, transactions);
		}

		return accounts;
	}

	/**
	 * Get a account with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return Account
	 */
	public Account calculateAccountAmounts(Account account, List<Transaction> transactions) {

		BigDecimal incomes = transactions.stream().filter(p -> p.getCategory().getType() == CategoryType.Income)
				.filter(p -> p.getAccount().getId() == account.getId()).filter(p -> p.isPayOrNotPay())
				.map(Transaction::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

		BigDecimal expenses = transactions.stream().filter(p -> p.getCategory().getType() == CategoryType.Expense)
				.filter(p -> p.getAccount().getId() == account.getId()).filter(p -> p.isPayOrNotPay())
				.map(Transaction::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

		account.setActualBalance(incomes.subtract(expenses).add(account.getInitialBalance()));

		return account;
	}
}
