package com.financial.business;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

public class LogicToEditTransaction {

	ExceptionBox exceptionBox;

	/**
	 * Resolves the edit of the Transaction object and returns true if everything is OK
	 * 
	 * @author Rodrigo Farias
	 * @return boolean
	 */
	public boolean editTransaction(int id, int payOrNotPay, String value, String transactionDueDate, String description,
			int account, int category, int method, User logged, TransactionRepository transactionRepository,
			CategoryRepository categoryRepository, MethodRepository methodRepository,
			AccountRepository accountRepository) {

		List<Transaction> list = new ArrayList<>();

		Transaction transaction = transactionRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

		transaction.setValue(new FeatureBox().getMoneyFromHtml(value));
		transaction.setTransactionDueDate(transactionDueDate);
		transaction.setDescription(new FeatureBox().convertUTF8toISO(description));

		if (transaction.getAccount().getId() != account) {
			transaction.setAccount(accountRepository.findById(account)
					.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + account)));
		}

		if (transaction.getCategory().getId() != category) {
			transaction.setCategory(categoryRepository.findById(category)
					.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + category)));
		}

		if (transaction.getMethod().getId() != method) {
			transaction.setMethod(methodRepository.findById(method)
					.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + method)));
		}

		if (payOrNotPay == 1) {

			transaction.setPayOrNotPay(true);
			transaction.setTransactionPaymentDate(new CalendarBox().getStringDate());
			transactionRepository.save(transaction);
			return true;

		} else if (payOrNotPay == 2) {

			transaction.setPayOrNotPay(false);
			transactionRepository.save(transaction);
			return true;

		} else if (payOrNotPay == 3) {

			list.addAll(transactionRepository.getAllByRecurringId(transaction.getRecurringId()).stream()
					.filter(p -> p.getId() != transaction.getId()).collect(Collectors.toList()));

			list.forEach(u -> u.setAccount(transaction.getAccount()));
			list.forEach(u -> u.setCategory(transaction.getCategory()));
			list.forEach(u -> u.setMethod(transaction.getMethod()));

			list.add(transaction);
			list.forEach(u -> u.setPayOrNotPay(true));
			transactionRepository.saveAll(list);

			return true;

		} else if (payOrNotPay == 4) {

			list.addAll(transactionRepository.getAllByRecurringId(transaction.getRecurringId()).stream()
					.filter(p -> p.getId() != transaction.getId()).collect(Collectors.toList()));

			list.forEach(u -> u.setAccount(transaction.getAccount()));
			list.forEach(u -> u.setCategory(transaction.getCategory()));
			list.forEach(u -> u.setMethod(transaction.getMethod()));

			list.add(transaction);
			list.forEach(u -> u.setPayOrNotPay(false));
			transactionRepository.saveAll(list);

			return true;

		}

		return false;
	}
}
