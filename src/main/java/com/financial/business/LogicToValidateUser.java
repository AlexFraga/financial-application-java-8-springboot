package com.financial.business;

import java.util.Optional;

import com.financial.enumarations.Status;
import com.financial.model.User;
import com.financial.repositories.UserRepository;
import com.financial.utilities.ExceptionBox;

public class LogicToValidateUser {

	ExceptionBox exceptionBox;

	/**
	 * Returns true if the user is Active
	 * 
	 * @author Rodrigo Farias
	 * @return boolean
	 */
	public boolean validateUser(UserRepository userRepository, User logged) {
		
		Optional<User> object = userRepository.findById(logged.getId());
		
		if(object.get().getStatus() == Status.INACTIVE) return false;
		
		return true;
	}
}
