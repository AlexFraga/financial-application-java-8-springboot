package com.financial.business;

import java.math.BigDecimal;
import java.util.List;

import com.financial.enumarations.CategoryType;
import com.financial.model.Account;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;

/**
 * Class representing statistics used on dashboard top
 */
public class LogicToBuildDashboardTop {

	ExceptionBox exceptionBox;

	private BigDecimal sumAccounts;
	private BigDecimal incomes;
	private BigDecimal expenses;

	// contructors

	public LogicToBuildDashboardTop() {

	}

	public LogicToBuildDashboardTop(BigDecimal sumAccounts, BigDecimal incomes, BigDecimal expenses) {
		this.sumAccounts = sumAccounts;
		this.incomes = incomes;
		this.expenses = expenses;
	}

	// methods

	public LogicToBuildDashboardTop getStatistics(AccountRepository accountRepository,
			TransactionRepository transactionRepository, User logged) {

		List<Account> accounts = new BusinessAccount().getAccounts(accountRepository,
				transactionRepository, logged);

		List<Transaction> transactions = transactionRepository.getAllByUser(logged);

		return getStatisticsWithCalculatedAmounts(accounts, transactions);

	}

	public LogicToBuildDashboardTop getStatisticsWithCalculatedAmounts(List<Account> accounts,
			List<Transaction> transactions) {

		try {
			// Resolves the sum of the total of all accounts
			BigDecimal sum = accounts.stream().filter(p -> p.isIncludeSumOnDashboard() == true)
					.map(Account::getActualBalance).reduce(BigDecimal.ZERO, BigDecimal::add);

			// Resolves the sum of the incomes from actual month
			BigDecimal incomes = transactions
					.stream()
					.filter(p -> p.getCategory().getType() == CategoryType.Income)
					.filter(p -> p.isPayOrNotPay())
					.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
					.map(Transaction::getValue)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			// Resolves the sum of the expense from actual month
			BigDecimal expenses = transactions.stream()
					.filter(p -> p.getCategory().getType() == CategoryType.Expense)
					.filter(p -> p.isPayOrNotPay())
					.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
					.map(Transaction::getValue)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			return new LogicToBuildDashboardTop(sum, incomes, expenses);

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}

	// getters and setters

	public BigDecimal getSumAccounts() {
		return sumAccounts;
	}

	public void setSumAccounts(BigDecimal sumAccounts) {
		this.sumAccounts = sumAccounts;
	}

	public BigDecimal getIncomes() {
		return incomes;
	}

	public void setIncomes(BigDecimal incomes) {
		this.incomes = incomes;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}
}
