package com.financial.business;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.financial.enumarations.CategoryType;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;

/**
 * Class to populate dashboard
 */
public class LogicToBuildDashboard {

	ExceptionBox exceptionBox;

	public List<Object> getDashboardData(AccountRepository accountRepository,
			TransactionRepository transactionRepository, CategoryRepository categoryRepository,
			MethodRepository methodRepository, User logged) {

		try {

			List<Object> objects = new ArrayList<>();

			List<Account> accounts = accountRepository.getAllByUser(logged);
			List<Transaction> transactions = transactionRepository.getAllByUser(logged);
			List<Category> categories = categoryRepository.getAllByUser(logged);
			List<Method> methods = methodRepository.getAllByUser(logged);

			// resolving accounts
			List<Account> calculatedAccounts = new LogicToCalculateAccountAmounts()
					.getAccountsWithCalculatedAmounts(accounts, transactions);

			// resolving dashboard top
			LogicToBuildDashboardTop dashboardTopObject = new LogicToBuildDashboardTop()
					.getStatisticsWithCalculatedAmounts(calculatedAccounts, transactions);

			// resolving incomes and expenses list
			List<Category> calculatedCategories = new LogicToCalculateCategoryAmounts()
					.getCategoriesWithCalculatedAmounts(categories, transactions);

			List<Category> incomes = categories.stream().filter(p -> p.getType() == CategoryType.Income)
					.collect(Collectors.toCollection(ArrayList::new));

			List<Category> expenses = categories.stream().filter(p -> p.getType() == CategoryType.Expense)
					.collect(Collectors.toCollection(ArrayList::new));

			// resolving methods
			List<Method> calculatedMethods = new LogicToCalculateMethodAmounts()
					.getMethodsWithCalculatedAmounts(methods, transactions);

			// resolving main transaction list on dashboard, NOTE must be the last one
			transactions.forEach(
					u -> u.setTransactionDueDate(new CalendarBox().convertStringDate(u.getTransactionDueDate())));

			objects.add(calculatedAccounts); // first -> 0
			objects.add(dashboardTopObject); // second -> 1
			objects.add(incomes); // third -> 2
			objects.add(expenses); // fourth -> 3
			objects.add(calculatedCategories); // fifth -> 4
			objects.add(calculatedMethods); // sixth -> 5
			objects.add(transactions); // seventh -> 6

			return objects;

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}
	}
}
