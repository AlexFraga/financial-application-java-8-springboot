package com.financial.business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.financial.enumarations.RecurringPeriod;
import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.Transaction;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.EnumManagement;
import com.financial.utilities.ExceptionBox;
import com.financial.utilities.FeatureBox;

public class LogicToBuildTransaction {

	ExceptionBox exceptionBox;

	/**
	 * Resolves the add of the Transaction object and returns true if everything is OK
	 * 
	 * @author Rodrigo Farias
	 * @return boolean
	 */
	public boolean buildTransaction(boolean payOrNotPay, String value, String transactionDueDate, String recurrent,
			int quantity, String description, boolean isItRecurring, String postType, String account, String category,
			String method, User logged, TransactionRepository transactionRepository,
			CategoryRepository categoryRepository, MethodRepository methodRepository,
			AccountRepository accountRepository) {

		try {

			Calendar c = Calendar.getInstance();
			c.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(transactionDueDate));
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));

			Account acc = getAccount(account, logged, accountRepository);
			Category cat = getCategory(category, postType, logged, categoryRepository);
			Method met = getMethod(method, logged, methodRepository);

			String recurringId = new FeatureBox().getRandomCombinationOfCharacters(16);
			RecurringPeriod rc = new EnumManagement().getRecurrentPeriod(recurrent);
			List<Transaction> list = new ArrayList<>();

			list.add(new Transaction(value, payOrNotPay, transactionDueDate, description, isItRecurring, recurringId,
					rc, acc, met, cat, logged)); // add first one

			if (isItRecurring == true) {

				if (rc == RecurringPeriod.Daily) {

					c.add(Calendar.DAY_OF_MONTH, 2);
					list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description, isItRecurring,
							recurringId, rc, acc, met, cat, logged)); // add on list

					for (int i = 1; i < quantity; i++) {

						c.add(Calendar.DAY_OF_MONTH, 1);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Weekly) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.WEEK_OF_MONTH, 1);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Fortnightly) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.DAY_OF_MONTH, 15);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Monthly) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.MONTH, 1);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Quarterly) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.MONTH, 3);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Semiannually) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.MONTH, 6);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);

				} else if (rc == RecurringPeriod.Annually) {

					for (int i = 0; i < quantity; i++) {

						c.add(Calendar.YEAR, 1);

						list.add(new Transaction(value, false, dateFormat.format(c.getTime()), description,
								isItRecurring, recurringId, rc, acc, met, cat, logged)); // add on list

					}

					transactionRepository.saveAll(list);
				}

			} else {

				transactionRepository.save(new Transaction(value, payOrNotPay, transactionDueDate, description,
						isItRecurring, recurringId, null, acc, met, cat, logged));
			}

			return true;

		} catch (ParseException e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return false;
		}
	}

	public Account getAccount(String account, User logged, AccountRepository accountRepository) {

		if (new FeatureBox().isNumeric(account) == true) {
			return accountRepository.findById(Integer.parseInt(account)).get();
		} else {
			return accountRepository.save(new Account(account, "0.00", true, logged));
		}
	}

	public Category getCategory(String category, String postType, User logged, CategoryRepository categoryRepository) {

		if (new FeatureBox().isNumeric(category) == true) {
			return categoryRepository.findById(Integer.parseInt(category)).get();
		} else {
			return categoryRepository.save(new Category(category, postType, logged));
		}
	}

	public Method getMethod(String method, User logged, MethodRepository methodRepository) {

		if (new FeatureBox().isNumeric(method) == true) {
			return methodRepository.findById(Integer.parseInt(method)).get();
		} else {
			return methodRepository.save(new Method(method, logged));
		}
	}
}
