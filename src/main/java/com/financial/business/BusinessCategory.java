package com.financial.business;

import java.util.List;

import com.financial.model.Category;
import com.financial.model.User;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.TransactionRepository;
import com.financial.utilities.ExceptionBox;

public class BusinessCategory {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of categories with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Category>
	 */
	public List<Category> getCategories(CategoryRepository categoryRepository,
			TransactionRepository transactionRepository, User logged) {

		return new LogicToCalculateCategoryAmounts().getCategoriesWithCalculatedAmounts(
				categoryRepository.getAllByUser(logged), transactionRepository.getAllByUser(logged));
	}
}
