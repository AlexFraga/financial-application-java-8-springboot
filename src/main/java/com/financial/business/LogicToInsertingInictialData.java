package com.financial.business;

import com.financial.model.Account;
import com.financial.model.Category;
import com.financial.model.Method;
import com.financial.model.User;
import com.financial.repositories.AccountRepository;
import com.financial.repositories.CategoryRepository;
import com.financial.repositories.MethodRepository;
import com.financial.repositories.TransactionRepository;

public class LogicToInsertingInictialData {

	public void insertingInictialData(User user, AccountRepository accountRepository,
			TransactionRepository transactionRepository, CategoryRepository categoryRepository,
			MethodRepository methodRepository) {

		accountRepository.save(new Account("My First Bank Account", "0.00", true, user));
		accountRepository.save(new Account("My First Wallet", "0.00", true, user));

		categoryRepository.save(new Category("Education", "0", user));
		categoryRepository.save(new Category("Food", "0", user));
		categoryRepository.save(new Category("Recreation", "0", user));
		categoryRepository.save(new Category("Transport", "0", user));

		categoryRepository.save(new Category("Education", "1", user));
		categoryRepository.save(new Category("Food", "1", user));
		categoryRepository.save(new Category("Recreation", "1", user));
		categoryRepository.save(new Category("Transport", "1", user));

		methodRepository.save(new Method("Bank Transfer", user));
		methodRepository.save(new Method("Bank Slip", user));
		methodRepository.save(new Method("PayPal", user));
		methodRepository.save(new Method("Google Pay", user));
		methodRepository.save(new Method("WePay", user));
		methodRepository.save(new Method("Skrill", user));
		methodRepository.save(new Method("Dwolla", user));
	}
}
