package com.financial.business;

import java.math.BigDecimal;
import java.util.List;

import com.financial.model.Category;
import com.financial.model.Transaction;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;

public class LogicToCalculateCategoryAmounts {

	ExceptionBox exceptionBox;

	/**
	 * Get a list of categories with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return List<Category>
	 */
	public List<Category> getCategoriesWithCalculatedAmounts(List<Category> categories,
			List<Transaction> transactions) {

		for (Category object : categories) {
			calculateCategoryAmounts(object, transactions);
		}

		return categories;
	}

	/**
	 * Get a category with their calculated values
	 * 
	 * @author Rodrigo Farias
	 * @return Category
	 */
	public Category calculateCategoryAmounts(Category category, List<Transaction> transactions) {
		
		BigDecimal paidMonthlyMovement = transactions
				.stream()
				.filter(p -> p.getCategory().getId() == category.getId())
				.filter(p -> p.isPayOrNotPay())
				.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
				.map(Transaction::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		BigDecimal toPayMonthlyMovement = transactions
				.stream()
				.filter(p -> p.getCategory().getId() == category.getId())
				.filter(p -> p.isPayOrNotPay() == false)
				.filter(p -> new CalendarBox().compareMonthAndYearAreCurrent(p.getTransactionDueDate()))
				.map(Transaction::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		category.setPaidMonthlyMovement(paidMonthlyMovement);
		category.setToPayMonthlyMovement(toPayMonthlyMovement);

		return category;
	}
}
