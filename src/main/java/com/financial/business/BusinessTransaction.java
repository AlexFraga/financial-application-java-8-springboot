package com.financial.business;

import java.util.List;

import com.financial.model.Transaction;
import com.financial.utilities.CalendarBox;
import com.financial.utilities.ExceptionBox;

public class BusinessTransaction {

	ExceptionBox exceptionBox;

	public List<Transaction> getTransactions(List<Transaction> transactions) {

		try {
			
			transactions.forEach(
					u -> u.setTransactionDueDate(new CalendarBox().convertStringDate(u.getTransactionDueDate())));

		} catch (Exception e) {
			exceptionBox.getAnExceptionAndDoTheTreatment(e);
			e.printStackTrace();
			return null;
		}

		return transactions;

	}
}
