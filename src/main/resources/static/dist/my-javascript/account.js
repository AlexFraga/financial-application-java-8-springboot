$('#add-account').on('hidden.bs.modal', function (e) {
	document.getElementById("form-add-account").reset(); // reset form
});

$('#edit-account').on('hidden.bs.modal', function (e) {
	document.getElementById("form-edit-account").reset(); // reset form
});

function editAccount(id){
	
	$.getJSON("http://localhost:8080/getaccount/" + id + "", function(json) {
		
		document.getElementById('edit-account-id').value = json.id;
		document.getElementById('edit-account-name').value = json.name;
		document.getElementById('initialBalance').value = json.initialBalance;
		document.getElementById('includeSumOnDashboard').value = json.includeSumOnDashboard;
	});
}

function deleteAccount(id) {
	
    swal({   
        title: "Do you want to delete this account?",   
        text: "This operation is irreversible!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function(){   
        swal("Deleted!", "Deleted successfully.", "success");
        window.location.href = '/delete-account?id=' + id;
    });
}

$("#accounts-table-search").on("keyup", function() {
	var value = $(this).val().toLowerCase();
	$("#accounts-table tr").filter(function() {
		$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	});
});