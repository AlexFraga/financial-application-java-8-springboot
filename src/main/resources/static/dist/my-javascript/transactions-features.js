$(document).ready(function() {

	getActualMonthAndYear(); // used to set date in the transaction table dropdown on the dashboard
	getToday(); // used to date the top of the transaction table on the dashboard
	filterTransactions();
});

function getActualMonthAndYear(){ // used to set date in the transaction table dropdown on the dashboard	
	const monthYear = moment().format("MMMM, YYYY");
	document.getElementById('transactions-month-year-select').value = monthYear;	
}

function getToday(){ // used to date the top of the transaction table on the dashboard	
	$("#transactions-date-table").text(moment().format("dddd, DD MMMM YYYY"));
}

$("#transactions-month-year-select").change(function() { // checks the change in the drop-down dates
	filterTransactions();
});

$("#transactions-category-select").change(function() { // checks the change in categories drop down
	changeCategoryClass();
	filterTransactions();
});

$("#transactions-status-select").change(function() { // checks the change in status drop down
	changeStatusClass();
	filterTransactions();
});

function filterTransactions(){
	
	var boxSelectedMonth = $("#transactions-month-year-select option:selected").val();
	var boxSelectedCategory = $("#transactions-category-select option:selected").val();
	var boxSelectedStatus = $("#transactions-status-select option:selected").val();
	
	var value = parseFloat("0.00").toFixed(2); // variable that stores the total value of the transactions of the selected month
	$('#transactions-table').find('tr').hide(); // clean the table
	
	$('#transactions-table tr').each(function(){ // consuming transaction table
	
		// months
		var date = $(this).find(".transactions-table-date").html(); // retrieving table dates
		var momentDate = moment(date, "DD/MM/YYYY").format("MMMM, YYYY"); // converting the date of the table and comparing with the current month and year
		
		// category
		var c = $(this).find(".transactions-table-category-type").html(); // retrieving table categories
		var category = c == 0 ? 1 : 2;
		
		// status
		var s = $(this).find(".transactions-table-paynotpay").html(); // retrieving table status
		var status = s == 'true' ? 1 : 2;
		
		// bad logic with a lot of if HAHAHAHA!
		if(boxSelectedCategory == 0 && boxSelectedStatus != 0) {

			if(boxSelectedMonth == momentDate && boxSelectedStatus == status){
	            $(this).show();
				
				var valueFromHtmlWithCurrency = $(this).find(".transactions-table-value").html(); // retrieving table values
				var valueFromHtml = valueFromHtmlWithCurrency.substring(2, valueFromHtmlWithCurrency.length); // remove -> $
				value = (parseFloat(value) + parseFloat(valueFromHtml)).toFixed(2); // calculating totals
				
			} else {
				$(this).hide();
			}
			
		} else if(boxSelectedCategory != 0 && boxSelectedStatus == 0){

			if(boxSelectedMonth == momentDate && boxSelectedCategory == category){
	            $(this).show();
				
	            var valueFromHtmlWithCurrency = $(this).find(".transactions-table-value").html(); // retrieving table values
				var valueFromHtml = valueFromHtmlWithCurrency.substring(2, valueFromHtmlWithCurrency.length); // remove -> $
				value = (parseFloat(value) + parseFloat(valueFromHtml)).toFixed(2); // calculating totals
				
			} else {
				$(this).hide();
			}
			
		} else if(boxSelectedCategory == 0 && boxSelectedStatus == 0){

			if(boxSelectedMonth == momentDate){
	            $(this).show();
				
	            var valueFromHtmlWithCurrency = $(this).find(".transactions-table-value").html(); // retrieving table values
				var valueFromHtml = valueFromHtmlWithCurrency.substring(2, valueFromHtmlWithCurrency.length); // remove -> $
				value = (parseFloat(value) + parseFloat(valueFromHtml)).toFixed(2); // calculating totals
				
			} else {
				$(this).hide();
			}
			
		} else if(boxSelectedCategory != 0 && boxSelectedStatus != 0){

			if(boxSelectedMonth == momentDate && boxSelectedCategory == category && boxSelectedStatus == status){
	            $(this).show();
				
	            var valueFromHtmlWithCurrency = $(this).find(".transactions-table-value").html(); // retrieving table values
				var valueFromHtml = valueFromHtmlWithCurrency.substring(2, valueFromHtmlWithCurrency.length); // remove -> $
				value = (parseFloat(value) + parseFloat(valueFromHtml)).toFixed(2); // calculating totals
				
			} else {
				$(this).hide();
			}			
		}
		
	});
	
	$('#transactions-value-total').text("$ " + value); // setting total
}

function changeCategoryClass(){
	var boxSelectedCategory = $("#transactions-category-select option:selected").val();
	
	if(boxSelectedCategory == 0){
		removeCategoryClass();
		 
	} else if(boxSelectedCategory == 1){
		removeCategoryClass();
		$("#select-category-class-on-transactions").addClass("has-success");
		$("#transactions-value-total").addClass("text-success");
		
	} else if(boxSelectedCategory == 2){
		removeCategoryClass();
		$("#select-category-class-on-transactions").addClass("has-danger");
		$("#transactions-value-total").addClass("text-danger");
	}	
}

function changeStatusClass(){
	var boxSelectedStatus = $("#transactions-status-select option:selected").val();
	
	if(boxSelectedStatus == 0){
		removeStatusClass();
		 
	} else if(boxSelectedStatus == 1){
		removeStatusClass();
		$("#select-status-class").addClass("has-success");
		
	} else if(boxSelectedStatus == 2){
		removeStatusClass();
		$("#select-status-class").addClass("has-danger");
	}	
}

function removeCategoryClass(){ // just remove class
	$("#select-category-class-on-transactions").removeClass("has-success");
	$("#select-category-class-on-transactions").removeClass("has-danger");
	
	$("#transactions-value-total").removeClass("text-success");
	$("#transactions-value-total").removeClass("text-danger");
}

function removeStatusClass(){ // just remove class
	$("#select-status-class").removeClass("has-success");
	$("#select-status-class").removeClass("has-danger");
}

$("#transactions-table-search").on("keyup", function() {
	var value = $(this).val().toLowerCase();
	$("#transactions-table tr").filter(function() {
		$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	});
});




