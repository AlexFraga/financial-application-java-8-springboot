$(document).ready(function() {

	$('.maskmoneyclass').maskMoney();
	messageToastr();

});

function messageToastr() {

	var value = $("#messagetoastr").val().toLowerCase();
	var text = $("#texttoastr").val();

	if (value == '1') {
		
		$.toast({
            heading: 'Success'
            , text: text
            , position: 'bottom-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
	    
	} else if (value == '2') {

		$.toast({
            heading: 'Error'
            , text: text
            , position: 'bottom-left'
            , loaderBg: '#ff6849'
            , icon: 'error'
            , hideAfter: 3500
            , stack: 6
        })
		
	} else if (value == '3') {

		$.toast({
            heading: 'Info'
            , text: text
            , position: 'bottom-left'
            , loaderBg: '#ff6849'
            , icon: 'info'
            , hideAfter: 3500
            , stack: 6
        })
		
	} else {
		
	}
}

$(function () {
    $('#myTable').DataTable();
    $(function () {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});