$('#transaction-income-add').on('hidden.bs.modal', function (e) {
	document.getElementById("form-add-income-transaction").reset(); // reset form
	existingAddIncomeAccount();
	existingAddIncomeCategory();
	existingAddIncomeMethod();
});

$('#transaction-expense-add').on('hidden.bs.modal', function (e) {
	document.getElementById("form-add-expense-transaction").reset(); // reset form
	existingAddExpenseAccount();
	existingAddExpenseCategory();
	existingAddExpenseMethod();
});

$('#transaction-edit').on('hidden.bs.modal', function (e) {
	document.getElementById("form-edit-transaction").reset(); // reset form
	
	$("#edit-account-category-income-box").hide();
	document.getElementById('edit-account-category-income').name = '';
	document.getElementById("edit-account-category-income").required = false;
	
	$("#edit-account-category-expense-box").hide();
	document.getElementById('edit-account-category-expense').name = '';
	document.getElementById("edit-account-category-expense").required = false;
});

function editTransaction(id, categoryType){
	
	if(categoryType == 0){
		
		$("#edit-transaction-category-income-box").show();
		document.getElementById('edit-transaction-category-income').name = 'category';
		document.getElementById("edit-transaction-category-income").required = true;
		
		$("#edit-transaction-category-expense-box").hide();
		document.getElementById('edit-transaction-category-expense').name = '';
		document.getElementById("edit-transaction-category-expense").required = false;
		
		$.getJSON("http://localhost:8080/gettransaction/" + id + "", function(json) {
			
			var payornot = json.payOrNotPay == true ? 1 : 2;
			document.getElementById('edit-transaction-id').value = json.id;
			document.getElementById('edit-transaction-value').value = json.value;
			document.getElementById('edit-transaction-duedate').value = json.transactionDueDate;
			document.getElementById('edit-transaction-account').value = json.account.id;
			document.getElementById('edit-transaction-category-income').value = json.category.id;
			document.getElementById('edit-transaction-method').value = json.method.id;
			document.getElementById('edit-transaction-payornotpay').value = payornot;
			document.getElementById('edit-transaction-description').value = json.description;
		});
		
	} else if(categoryType == 1){
		
		$("#edit-transaction-category-expense-box").show();
		document.getElementById('edit-transaction-category-expense').name = 'category';
		document.getElementById("edit-transaction-category-expense").required = true;
		
		$("#edit-transaction-category-income-box").hide();
		document.getElementById('edit-transaction-category-income').name = '';
		document.getElementById("edit-transaction-category-income").required = false;
		
		$.getJSON("http://localhost:8080/gettransaction/" + id + "", function(json) {
			
			var payornot = json.payOrNotPay == true ? 1 : 2;
			document.getElementById('edit-transaction-id').value = json.id;
			document.getElementById('edit-transaction-value').value = json.value;
			document.getElementById('edit-transaction-duedate').value = json.transactionDueDate;
			document.getElementById('edit-transaction-account').value = json.account.id;
			document.getElementById('edit-transaction-category-expense').value = json.category.id;
			document.getElementById('edit-transaction-method').value = json.method.id;
			document.getElementById('edit-transaction-payornotpay').value = payornot;
			document.getElementById('edit-transaction-description').value = json.description;
		});
		
	}
}

function deleteTransaction(id) {
	
	document.getElementById('delete-transaction-id').value = id;
}

//add income modal
function newAddIncomeAccount(){
	$("#add-income-account-select-box").hide();
	$("#add-income-account-select").prop('disabled', true);
	document.getElementById('add-income-account-select').name = '';
	document.getElementById("add-income-account-select").required = false;
	
	$("#add-income-account-input-box").show();
	$("#add-income-account-input").prop('disabled', false);
	document.getElementById('add-income-account-input').name = 'account';
	document.getElementById("add-income-account-input").required = true;
}

function existingAddIncomeAccount(){
	$("#add-income-account-input-box").hide();
	$("#add-income-account-input").prop('disabled', true);
	document.getElementById('add-income-account-input').name = '';
	document.getElementById("add-income-account-input").required = false;
	
	$("#add-income-account-select-box").show();
	$("#add-income-account-select").prop('disabled', false);
	document.getElementById('add-income-account-select').name = 'account';
	document.getElementById("add-income-account-select").required = true;
}

function newAddIncomeCategory(){
	$("#add-income-category-select-box").hide();
	$("#add-income-category-select").prop('disabled', true);
	document.getElementById('add-income-category-select').name = '';
	document.getElementById("add-income-category-select").required = false;
	
	$("#add-income-category-input-box").show();
	$("#add-income-category-input").prop('disabled', false);
	document.getElementById('add-income-category-input').name = 'category';
	document.getElementById("add-income-category-input").required = true;
}

function existingAddIncomeCategory(){
	$("#add-income-category-input-box").hide();
	$("#add-income-category-input").prop('disabled', true);
	document.getElementById('add-income-category-input').name = '';
	document.getElementById("add-income-category-input").required = false;
	
	$("#add-income-category-select-box").show();
	$("#add-income-category-select").prop('disabled', false);
	document.getElementById('add-income-category-select').name = 'category';
	document.getElementById("add-income-category-select").required = true;
}

function newAddIncomeMethod(){
	$("#add-income-method-select-box").hide();
	$("#add-income-method-select").prop('disabled', true);
	document.getElementById("add-income-method-select").name = '';
	document.getElementById("add-income-method-select").required = false;
	
	$("#add-income-method-input-box").show();
	$("#add-income-method-input").prop('disabled', false);
	document.getElementById("add-income-method-input").name = 'method';
	document.getElementById("add-income-method-input").required = true;
}

function existingAddIncomeMethod(){
	$("#add-income-method-input-box").hide();
	$("#add-income-method-input").prop('disabled', true);
	document.getElementById("add-income-method-input").name = '';
	document.getElementById("add-income-method-input").required = false;
	
	$("#add-income-method-select-box").show();
	$("#add-income-method-select").prop('disabled', false);
	document.getElementById("add-income-method-select").name = 'method';
	document.getElementById("add-income-method-select").required = true;
}

$("#add-income-recurrent-is-it").change(function() {
	var boxSelected = $("#add-income-recurrent-is-it option:selected").val();
	
	if(boxSelected == 'true'){
		document.getElementById('add-income-quantity').value = 1;
		$("#add-income-recurrent-quantity").show();
		$("#add-income-recurrent-period").show();
		
		
	} else if(boxSelected == 'false') {
		document.getElementById('add-income-quantity').value = 1;
		$("#add-income-recurrent-quantity").hide();
		$("#add-income-recurrent-period").hide();
	}
});

//add expense modal
function newAddExpenseAccount(){
	$("#add-expense-account-select-box").hide();
	$("#add-expense-account-select").prop('disabled', true);
	
	$("#add-expense-account-input-box").show();
	$("#add-expense-account-input").prop('disabled', false);
}

function existingAddExpenseAccount(){
	$("#add-expense-account-input-box").hide();
	$("#add-expense-account-input").prop('disabled', true);
	
	$("#add-expense-account-select-box").show();
	$("#add-expense-account-select").prop('disabled', false);
}

function newAddExpenseCategory(){
	$("#add-expense-category-select-box").hide();
	$("#add-expense-category-select").prop('disabled', true);
	
	$("#add-expense-category-input-box").show();
	$("#add-expense-category-input").prop('disabled', false);
}

function existingAddExpenseCategory(){
	$("#add-expense-category-input-box").hide();
	$("#add-expense-category-input").prop('disabled', true);
	
	$("#add-expense-category-select-box").show();
	$("#add-expense-category-select").prop('disabled', false);
}

function newAddExpenseMethod(){
	$("#add-expense-method-select-box").hide();
	$("#add-expense-method-select").prop('disabled', true);
	
	$("#add-expense-method-input-box").show();
	$("#add-expense-method-input").prop('disabled', false);
}

function existingAddExpenseMethod(){
	$("#add-expense-method-input-box").hide();
	$("#add-expense-method-input").prop('disabled', true);
	
	$("#add-expense-method-select-box").show();
	$("#add-expense-method-select").prop('disabled', false);
}

$("#add-expense-recurrent-is-it").change(function() {
	var boxSelected = $("#add-expense-recurrent-is-it option:selected").val();
	
	if(boxSelected == 'true'){
		document.getElementById('add-expense-quantity').value = 1;
		$("#add-expense-recurrent-quantity").show();
		$("#add-expense-recurrent-period").show();		
		
	} else if(boxSelected == 'false') {
		document.getElementById('add-expense-quantity').value = 1;
		$("#add-expense-recurrent-quantity").hide();
		$("#add-expense-recurrent-period").hide();
	}
});