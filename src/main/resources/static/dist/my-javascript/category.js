$(document).ready(function() {
	
});

$('#add-category').on('hidden.bs.modal', function (e) {
	document.getElementById("form-add-category").reset(); // reset form
});

$('#edit-category').on('hidden.bs.modal', function (e) {
	document.getElementById("form-edit-category").reset(); // reset form
});

function editCategory(id){
	
	$.getJSON("http://localhost:8080/getcategory/" + id + "", function(json) {
		
		var type = json.type == 'Income' ? 0 : 1;
		document.getElementById('edit-category-id').value = json.id;
		document.getElementById('edit-category-name').value = json.name;
		document.getElementById('edit-category-type').value = type;		
	});
}

function deleteCategory(id) {
	
    swal({   
        title: "Do you want to delete this category?",   
        text: "This operation is irreversible!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function(){   
        swal("Deleted!", "Deleted successfully.", "success");
        window.location.href = '/delete-category?id=' + id;
    });
}

$("#categories-table-search").on("keyup", function() {
	var value = $(this).val().toLowerCase();
	$("#categories-table tr").filter(function() {
		$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	});
});

$("#categories-category-select").change(function() { // checks the change in categories drop down
	categoryClass();
	filterCategories();
});

function categoryClass(){
	var boxSelectedCategory = $("#categories-category-select option:selected").val();
	
	if(boxSelectedCategory == 0){
		removeCategory();
		 
	} else if(boxSelectedCategory == 1){
		removeCategory();
		$("#select-category-class-on-categories").addClass("has-success");
		
	} else if(boxSelectedCategory == 2){
		removeCategory();
		$("#select-category-class-on-categories").addClass("has-danger");
	}	
}

function removeCategory(){ // just remove class
	$("#select-category-class-on-categories").removeClass("has-success");
	$("#select-category-class-on-categories").removeClass("has-danger");
}

function filterCategories(){
	
	var boxSelectedCategory = $("#categories-category-select option:selected").val();
	
	var paidValue = parseFloat("0.00").toFixed(2); // variable that stores the total of the paids
	var pendingValue = parseFloat("0.00").toFixed(2); // variable that stores the total of pending
	$('#categories-table').find('tr').hide(); // clean the table
	
	$('#categories-table tr').each(function(){ // consuming transaction table
		
		// category
		var c = $(this).find(".categories-table-category-type").html(); // retrieving table categories
		var category = c == 0 ? 1 : 2;
		
		// bad logic with a lot of if HAHAHAHA!
		if(boxSelectedCategory == 0) {
			$(this).show();
			
			var paidValueFromHtmlWithCurrency = $(this).find(".categories-table-paid").html(); // retrieving table values
			var paidValueFromHtml = paidValueFromHtmlWithCurrency.substring(2, paidValueFromHtmlWithCurrency.length); // remove -> $
			paidValue = (parseFloat(paidValue) + parseFloat(paidValueFromHtml)).toFixed(2); // calculating totals
			
			var pendingValueFromHtmlWithCurrency = $(this).find(".categories-table-pending").html(); // retrieving table values
			var pendingValueFromHtml = pendingValueFromHtmlWithCurrency.substring(2, pendingValueFromHtmlWithCurrency.length); // remove -> $
			pendingValue = (parseFloat(pendingValue) + parseFloat(pendingValueFromHtml)).toFixed(2); // calculating totals
			
			$('#categories-value-paid').text("0.00"); // setting total
			$('#categories-value-pending').text("0.00"); // setting total
			
		} else if(boxSelectedCategory != 0){

			if(boxSelectedCategory == category){
	            $(this).show();
				
	            var paidValueFromHtmlWithCurrency = $(this).find(".categories-table-paid").html(); // retrieving table values
				var paidValueFromHtml = paidValueFromHtmlWithCurrency.substring(2, paidValueFromHtmlWithCurrency.length); // remove -> $
				paidValue = (parseFloat(paidValue) + parseFloat(paidValueFromHtml)).toFixed(2); // calculating totals
				
				var pendingValueFromHtmlWithCurrency = $(this).find(".categories-table-pending").html(); // retrieving table values
				var pendingValueFromHtml = pendingValueFromHtmlWithCurrency.substring(2, pendingValueFromHtmlWithCurrency.length); // remove -> $
				pendingValue = (parseFloat(pendingValue) + parseFloat(pendingValueFromHtml)).toFixed(2); // calculating totals
				
			} else {
				$(this).hide();
			}
			
			$('#categories-value-paid').text(paidValue); // setting total
			$('#categories-value-pending').text(pendingValue); // setting total
		}		
	});
}