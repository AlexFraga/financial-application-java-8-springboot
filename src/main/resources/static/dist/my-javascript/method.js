$('#add-method').on('hidden.bs.modal', function (e) {
	document.getElementById("form-add-method").reset(); // reset form
});

$('#edit-method').on('hidden.bs.modal', function (e) {
	document.getElementById("form-edit-method").reset(); // reset form
});

function editMethod(id){
	
	$.getJSON("http://localhost:8080/getmethod/" + id + "", function(json) {
		
		document.getElementById('edit-method-id').value = json.id;
		document.getElementById('edit-method-name').value = json.name;
	});
}

function deleteMethod(id) {
	
    swal({   
        title: "Do you want to delete this method?",   
        text: "This operation is irreversible!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function(){   
        swal("Deleted!", "Deleted successfully.", "success");
        window.location.href = '/delete-method?id=' + id;
    });
}

$("#methods-table-search").on("keyup", function() {
	var value = $(this).val().toLowerCase();
	$("#methods-table tr").filter(function() {
		$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	});
});